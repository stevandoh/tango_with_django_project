from django.shortcuts import render

from django.http import HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from rango.models import Page
from rango.forms import CategoryForm, PageForm

#Import the category model
from rango.models import Category

def encode_url(value):
	return value.replace('','_')


def decode_url(value):
	return value.replace('_','')

def index(request):
	context = RequestContext(request)
	#Query the database for a list of all categories
	category_list = Category.objects.order_by('-likes')[:5]
	page_list  = Page.objects.order_by('-likes')[:5]
	context_dict = {'categories': category_list,'pages':page_list}


	for category in category_list:
		category.url = encode_url(category.name)

	#Render the response and send it back !
	return render_to_response('rango/index.html',context_dict,context)



def category(request,category_name_url):
	#Request our context from the request passed to us.

	context = RequestContext(request)
	#Change underscores in the caegory name to spaces

	#Change underscores in the name category name to spaces
	#URLs don't handle spaces well, so encode them as underscores
	#We can then simply replace the underscores with spaces

	category_name = decode_url(category_name_url)

	context_dict ={'category_name':category_name}

	try:
		category = Category.objects.get(name= category_name)
		pages = Page.objects.filter(category = category)

		context_dict['pages']= pages
		context_dict['category'] = category

	except Category.DoesNotExist:
		#Don't do anything

		pass

	return render_to_response('rango/category.html',context_dict,context)




def add_category(request):
	context =RequestContext(request)

	if request.method == 'POST':
		form = CategoryForm(request.POST)

		if form.is_valid():
			form.save(commit =True)

			'''
			Now call the index() view 
			The user will be shown the homepage
			'''
			return index(request)

		else:
			print form.errors
			#If the request was not a POST , display the form to enter details

	else:
		form =CategoryForm()
	return render_to_response('rango/add_category.html',{'form':form},context)



def add_page(request,category_name_url):
	context = RequestContext(context)
	category_name = decode_url(category_name_url)
	if request.method == 'POST':
		form = PageForm(request.POST)

		if form.is_valid():
			page = form.save(commit=False)
			try:
				cat =Category.objects.get(name=category_name)
				page.category = cat
			except Category.DoesNotExist:
				return render_to_response('rango/add_category.html',{},context)

			page.views = 0

			page.save()
			return category(request, category_name_url)
		else:
			print form.errors
	else:
		form = PageForm()

	return render_to_response('rango/add_page.html',{'category_name_url':category_name_url,
		'category_name':category_name,'form':form},context)





	